# README

This is an API that can be used to store data and search for matching insurance plans. A user could submit a plan in the form of any JSON object and it can be stored. Attributes on that plan can be used to search against a verified database of existing plans to find any possible matches.

Key details:

* Fuzzy search is used. Attributes are matched based on DICE coefficient/Levenshtein distance

* Search is completed against a flattened collection of active record attributes; A match can be returned for a match on any column's value

* All submissions are stored as JSONB objects, so future improvements to searching (ie, by select columns instead of the current method a general search) should be simple

* Deployment is handled through a docker script to heroku (NOTE: heroku is not currently set up; simply need to create heroku app and add variables to gitlab to set this up). Check out .gitlab-ci.yaml for more details

Decision making process:

Fuzzy search vs setting columns:
* The current search is done by turning active record entries into hashes, then flattening and fuzzy searching against the values. We return ids of any matches hashes and use those to return matches records. This is obviously considerably slower than searching against set columns, but does afford the flexibility of searching for anything. Would love to set up a websocket so that a front-end could take full advantage of this by returning results in real time as the user types

Storing data in jsonb columns:
* This isn't ideal and even if I keep fuzzy searching against everything, I'd love to have set columns if possible. I simply didn't have time to become familiar enough with the possible data to comfortably set this up


I was unsure if I also needed to complete a small user-interface for the project, but I set up all the routing and CORS configuration to make this easy if it was required. Given the way I set up my controllers, all I would have needed to do is set up a form to send a request with whatever search params I wanted. I set up my new routes to send called actions from a front-end straight to the relevant model, so to find a matched authenticated plan for a given submission, a user would just need to send the a search needle. Given resource constraints, I would have just deployed my code to an S3 bucket to set up a static site without needing to worry about having a domain/routing/etc.

