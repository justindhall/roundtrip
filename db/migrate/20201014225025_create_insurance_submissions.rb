class CreateInsuranceSubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :insurance_submissions, id: :uuid do |t|
      t.jsonb :submission
      t.uuid :authenticated_plan_id

      t.timestamps
    end
  end
end
