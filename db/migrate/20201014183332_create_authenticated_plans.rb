class CreateAuthenticatedPlans < ActiveRecord::Migration[6.0]
  enable_extension('pgcrypto') unless extensions.include?('pgcrypto')

  def change
    create_table :authenticated_plans, id: :uuid do |t|
      t.jsonb :plan
      t.string :member_number
      t.jsonb :company
      t.string :group_number
      t.string :group_name
      t.string :effective_date
      t.string :expiration_date
      t.string :policy_number
      t.string :agreement_type
      t.string :coverage_type
      t.jsonb :insured

      t.timestamps
    end
  end
end
