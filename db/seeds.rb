json = [
    {
        "plan": {
            "ID": "12345",
            "IDType": "Payor ID",
            "Name": "Deductible Plan",
            "Type": nil
        },
        "member_number": nil,
        "company": {
            "ID": "54321",
            "IDType": nil,
            "Name": "Athena (123 456)",
            "Address": {
                "StreetAddress": "PO Box 12345",
                "City": "Roundtrip Town",
                "State": "RT",
                "ZIP": "12345",
                "County": "Health County",
                "Country": "US"
            },
            "PhoneNumber": "+12223334444"
        },
        "group_number": "847025-123-4567",
        "group_name": "The Core Team",
        "effective_date": "2015-01-01",
        "expiration_date": "2020-12-31",
        "policy_number": "656835555",
        "agreement_type": nil,
        "coverage_type": nil,
        "insured": {
            "Identifiers": [],
            "LastName": nil,
            "FirstName": nil,
            "SSN": nil,
            "Relationship": nil,
            "DOB": nil,
            "Sex": nil,
            "Address": {
                "StreetAddress": nil,
                "City": nil,
                "State": nil,
                "ZIP": nil,
                "County": nil,
                "Country": nil
            }
        }
    },
    {
        "plan": {
            "ID": "12346",
            "IDType": "Payor ID",
            "Name": "PPO Plan",
            "Type": nil
        },
        "member_number": nil,
        "company": {
            "ID": "54321",
            "IDType": nil,
            "Name": "Athena Black",
            "Address": {
                "StreetAddress": "PO Box 12345",
                "City": "Roundtrip Town",
                "State": "RT",
                "ZIP": "12345",
                "County": "Health County",
                "Country": "US"
            },
            "PhoneNumber": "+12223334444"
        },
        "group_number": "847025-123-4567",
        "group_name": "The Core Team",
        "effective_date": "2015-01-01",
        "expiration_date": "2020-12-31",
        "policy_number": "656835555",
        "agreement_type": nil,
        "coverage_type": nil,
        "insured": {
            "Identifiers": [],
            "LastName": nil,
            "FirstName": nil,
            "SSN": nil,
            "Relationship": nil,
            "DOB": nil,
            "Sex": nil,
            "Address": {
                "StreetAddress": nil,
                "City": nil,
                "State": nil,
                "ZIP": nil,
                "County": nil,
                "Country": nil
            }
        }
    },
    {
        "plan": {
            "ID": "12347",
            "IDType": "Payor ID",
            "Name": "PPO Plan",
            "Type": nil
        },
        "member_number": nil,
        "company": {
            "ID": "54321",
            "IDType": nil,
            "Name": "Blue Cross",
            "Address": {
                "StreetAddress": "PO Box 12345",
                "City": "Roundtrip Town",
                "State": "RT",
                "ZIP": "12345",
                "County": "Health County",
                "Country": "US"
            },
            "PhoneNumber": "+12223334444"
        },
        "group_number": "847025-123-4567",
        "group_name": "The Core Team",
        "effective_date": "2015-01-01",
        "expiration_date": "2020-12-31",
        "policy_number": "656835555",
        "agreement_type": 'zzzzzzlongstringfortestingpurposes',
        "coverage_type": nil,
        "insured": {
            "Identifiers": [],
            "LastName": nil,
            "FirstName": nil,
            "SSN": nil,
            "Relationship": nil,
            "DOB": nil,
            "Sex": nil,
            "Address": {
                "StreetAddress": nil,
                "City": nil,
                "State": nil,
                "ZIP": nil,
                "County": nil,
                "Country": nil
            }
        }
    }
]

json.each do |entry|
  AuthenticatedPlan.create(entry)
end

# AuthenticatedPlan.where('plan ?| array[:values]', values: ['12345', 'Payor ID'])
# AuthenticatedPlan.where(plan.)
