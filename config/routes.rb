Rails.application.routes.draw do
  resources :authenticated_plans do
    get 'new', on: :collection
    post 'new', on: :collection
  end
end
