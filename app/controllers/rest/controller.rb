module Rest
  class Controller < ActionController::API
    include ActionController::HttpAuthentication::Token::ControllerMethods
    include Rest::Concerns::Parameters
    include Rest::Concerns::Pagination
    include Rest::Concerns::GracefulErrors

    before_action :initialize_model, only: :create
    before_action :validate_params

    def index
      response.headers['x-total'] = models_count
      response.headers['x-link-next'] = next_url unless next_url.nil?
      render json: models_remaining.limit(limit), each_serializer: serializer
    end

    def show
      render json: model, serializer: serializer
    end

    def new
      action = request.query_parameters[:action]
      render json: models.send(action, queryable_params), serializer: serializer
    end

    def create
      if model.save
        render json: model, serializer: serializer
      else
        render json: model.errors, status: :unprocessable_entity
      end
    end

    def update
      if model.update(modifiable_params)
        render json: model, serializer: serializer
      else
        render json: model.errors, status: :unprocessable_entity
      end
    end

    def destroy
      if model.destroy
        render json: model
      else
        render json: model.errors, status: :unprocessable_entity
      end
    end

    def destroy_by
      found_model = models.find_by(queryable_params)
      if !found_model
        head :not_found
      elsif found_model.destroy
        render json: found_model
      else
        render json: found_model.errors, status: :unprocessable_entity
      end
    end

    protected

    def model
      @model ||= models.find(queryable_params[:id])
    end

    def initialize_model
      @model = model_klass.new(modifiable_params)
    end

    def models
      model_klass.where('1 = 1')
    end

    def model_klass
      @model_klass ||= self.class.to_s.remove('Controller').singularize.constantize
    end

    def serializer
      "#{model_klass.name}Serializer".safe_constantize
    end
  end
end
