module Rest
  module Concerns
    module Parameters
      extend ActiveSupport::Concern
      included do

        before_action :validate_params

        protected

        def validate_params
          return unless not_allowed_params.present?
          render json: { base: "Extra parameters are not allowed: #{not_allowed_params.join(', ')}" }, status: :unprocessable_entity
        end

        def modifiable_params
          return @modifiable_params unless @modifiable_params.nil?
          @modifiable_params ||= params.permit!.to_h.select! { |key, _value| valid_params(modifiable_names).include?(key.to_sym) }
          @modifiable_params.keys.map do |key|
            if %w(undefined null).include? @modifiable_params[key]
              @modifiable_params[key] = nil
            end
          end
          @modifiable_params
        end

        def queryable_params
          return @queryable_params unless @queryable_params.nil?
          @queryable_params ||= params.permit!.to_h.select! { |key, _value| valid_params(queryable_names).include?(key.to_sym) }
          @queryable_params.keys.map do |key|
            if %w(undefined null).include? @queryable_params[key]
              @queryable_params[key] = nil
            end
          end
          @queryable_params
        end

        def valid_params(base_params)
          return clean(base_params)
        end

        def additional_valid_params
          []
        end

        def rest_valid_params
          [:pagination_param, :pagination_direction, :limit, :action]
        end

        def not_allowed_params
          @not_allowed_params ||= clean_request_names - (valid_params(queryable_names) + Array.wrap(rest_valid_params) + Array.wrap(additional_valid_params))
        end

        protected

        def clean_request_names
          params.keys.map(&:to_sym) - [:controller, :action, controller_name.to_sym, controller_name.singularize.to_sym]
        end

        def modifiable_names
          @modifiable_model_columns ||= clean(queryable_names - %i[created_at updated_at])
        end

        def queryable_names
          @queryable_model_columns ||= clean(model_klass.column_names.flatten)
        end

        def clean(attributes_to_clean)
          attributes_to_clean.map { |column_name| column_name.to_s.gsub('encrypted_', '').to_sym }.uniq
        end
      end
    end
  end
end
