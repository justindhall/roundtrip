module Rest
  module Concerns
    module GracefulErrors
      extend ActiveSupport::Concern

      included do
        rescue_from ActiveRecord::RecordNotFound do |_exception|
          render status: :not_found, json: nil
        end

        rescue_from Rest::Exception::BadRequest do |exception|
          render status: :bad_request, json: { base: 'Bad Request', message: exception.message }
        end

        rescue_from Rest::Exception::Unauthorized do |exception|
          render status: :forbidden, json: { base: "Access denied on #{exception.action} #{exception.controller}", message: exception.message }
        end
      end
    end
  end
end
