module Rest
  module Exception
    class Unauthorized < StandardError
      attr_reader :action
      attr_reader :controller
      def initialize(controller, action, message)
        super(message)
        @action = action
        @controller = controller
      end
    end
  end
end
