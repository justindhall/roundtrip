class AuthenticatedPlan < ApplicationRecord
  has_many :insurance_submissions

  def self.all_as_hashes
    all.to_a.map(&:serializable_hash)
  end

  def self.fuzzy_search(haystack, needle)
    FuzzyMatch.new(haystack).find(needle)
  end

  def self.search_for_matching_plan(needle)
    hashes = all_as_hashes
    matches = []
    hashes.each do |hash|
      flattened = flatten_hash(hash)
      next if fuzzy_search(flattened.values, needle).nil?
      matches << hash['id']
    end
    return [] if matches.empty?
    where(id: matches)
  end
end
