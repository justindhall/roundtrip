class InsuranceSubmission < ApplicationRecord
  belongs_to :authenticated_plan, optional: true

  def self.search_for_matching_plan_by_needle(params)
    submission = find(params[:id])
    results = AuthenticatedPlan.search_for_matching_plan(params[:needle])
    return if results.empty?
    return results if results.length > 1
    submission.update(authenticated_plan_id: results.first.id)
  end


  # was working on fuzzy searching by multiple terms at once to return a more likely result but ran out of time :/
  # def self.search_by_multiple_inputs(params)
  #   submission = find(params[:id])
  #   matches = []
  #   params[:search_terms].each do |term|
  #     matches << AuthenticatedPlan.search_for_matching_plan(term)
  #   end
  # end
end
