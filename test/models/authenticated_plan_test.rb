require 'test_helper'

class AuthenticatedPlanTest < ActiveSupport::TestCase
  test "fuzzy search should return result" do
    results = AuthenticatedPlan.search_for_matching_plan('12345')
    assert !results.nil?
  end
end
