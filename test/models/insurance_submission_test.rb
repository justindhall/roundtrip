require 'test_helper'

class InsuranceSubmissionTest < ActiveSupport::TestCase
  test "should find match and set an authenticated_plan_id" do
    submission = InsuranceSubmission.create({submission: {plan_id: '12345', plan_type:'zzzz'}})
    InsuranceSubmission.search_for_matching_plan_by_needle({id: submission.id, needle: 'zzzz'})
    assert !submission.authenticated_plan_id.nil?
  end
end
